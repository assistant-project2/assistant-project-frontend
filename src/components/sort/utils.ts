import { SortDirections } from '@/core/utils/sorting';

export const swapMap = {
  [SortDirections.ASC]: SortDirections.DESC,
  [SortDirections.DESC]: SortDirections.ASC,
};
export const apiPrefixMap = {
  [SortDirections.ASC]: '',
  [SortDirections.DESC]: '-',
};
