import { IData } from '@/core/types/base';

export interface IHeaderConfiguration<T = string> {
  key: T;
  name?: string;
  sortingField?: string;
  sortable?: boolean;
  flexGrow?: number;
  flexBasis?: string;
}

export type IHeaderKey<T> = keyof T | 'checkbox' | 'actions';

export interface TableProps {
  header: IHeaderConfiguration[];
  data: IData[];
  emptyTableText?: string;
  loading?: boolean;
  currentPage?: number;
  enableItemsPerPageDropdown?: boolean;
  total?: number;
  rowsPerPage?: number;
  order?: 'asc' | 'desc';
  sortLabel?: string;
}
