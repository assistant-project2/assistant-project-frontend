import Loader from '../AppLoader.vue';
import { Meta, StoryFn } from '@storybook/vue3';

interface LoaderProps {
  size: number;
  width: number;
  strokeColor: string;
  progress: number;
  rotating: boolean;
}

export default {
  title: 'Library/Components/Loader',
  component: Loader,
} as Meta<typeof Loader>;

const Template: StoryFn<LoaderProps> = (args) => ({
  components: { Loader },
  setup() {
    return { args };
  },
  template: '<Loader v-bind="args" />',
});

export const Default = Template.bind({});
