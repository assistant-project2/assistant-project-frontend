import Checkbox from '../form/CheckboxField.vue';
import { Meta, StoryFn } from '@storybook/vue3';

interface LoaderProps {
  positionLabel?: 'top' | 'bottom' | 'left' | 'right';
}

export default {
  title: 'Library/Components/Checkbox',
  component: Checkbox,
} as Meta<typeof Checkbox>;

const Template: StoryFn<LoaderProps> = (args) => ({
  components: { Checkbox },
  setup() {
    return { args };
  },
  template: '<Checkbox v-bind="args">Label</Checkbox>',
});

export const Default = Template.bind({});
