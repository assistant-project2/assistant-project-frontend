import Table from '../AppTable.vue';
import { Meta, StoryFn } from '@storybook/vue3';
import { TableProps } from '../types/table';

export default {
  title: 'Library/Components/Table',
  component: Table,
} as Meta<typeof Table>;

const Template: StoryFn<TableProps> = (args) => ({
  components: { Table },
  setup() {
    return { args };
  },
  template: `
    <div class="bg-white p-5">
      <Table v-bind="args">
        <template v-slot:cell-id="{ item }">
          <div class="form-check form-check-sm form-check-custom form-check-solid">
            <!-- <input class="form-check-input" type="checkbox" :value="position.id" v-model="checkedPositions" /> -->
            {{ item.id }}
          </div>
        </template>
        <template v-slot:cell-geo="{ item }"> {{ item.geo }} </template>
        <template v-slot:cell-week="{ item }"> {{ item.week }} </template>
      </Table>
    </div>
  `,
});

export const Default = Template.bind({});
Default.args = {
  loading: false,
  enableItemsPerPageDropdown: false,
  data: [
    {
      id: 1,
      geo: 'a',
      week: 12,
    },
    {
      id: 2,
      geo: 'b',
      week: 10,
    },
    {
      id: 3,
      geo: 'c',
      week: 12,
    },
    {
      id: 4,
      geo: 'd',
      week: 10,
    },
    {
      id: 5,
      geo: 'e',
      week: 10,
    },
  ],
  header: [
    {
      key: 'id',
      sortable: false,
    },
    {
      name: 'Когорта',
      key: 'geo',
      sortable: true,
    },
    {
      name: 'Неделя',
      key: 'week',
      sortable: true,
    },
  ],
};
