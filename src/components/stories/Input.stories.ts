import Input from '../form/TextField.vue';
import { Meta, StoryFn } from '@storybook/vue3';

interface InputProps {
  loading: boolean;
  number: boolean;
  hideControls: boolean;
  invalid: boolean;
  decimal: boolean;
  disabled: boolean;
  placeholder: string;
  min: number;
  max: number;
  light: boolean;
  small: boolean;
}

export default {
  title: 'Library/Components/Input',
  component: Input,
} as Meta<typeof Input>;

const Template: StoryFn<InputProps> = (args) => ({
  components: { Input },
  setup() {
    return { args };
  },
  template: `
    <Input v-bind="args">
    </Input>
  `,
});

export const Default = Template.bind({});
Default.args = {
  loading: false,
  number: false,
  hideControls: false,
  invalid: false,
  decimal: false,
  disabled: false,
  placeholder: 'placeholder',
  light: false,
  small: false,
};
