import AppIcon from '@/components/AppIcon.vue';
import { Meta, StoryFn } from '@storybook/vue3';

interface IProps {
  name: string;
}

export default {
  title: 'Library/Components/Icon',
  component: AppIcon,
} as Meta<typeof AppIcon>;

const Template: StoryFn<IProps> = (args) => ({
  setup() {
    return { args };
  },
  template: `
    <AppIcon v-bind="args" class="text-dark-blue-4 transition-colors hover:text-pink-primary" style="font-size: 263px" />
  `,
});

export const Default = Template.bind({});
Default.args = {
  name: 'amma',
};
