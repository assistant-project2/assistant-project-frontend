import AppPagination from '../AppPagination.vue';
import { Meta, StoryFn } from '@storybook/vue3';
import { ref } from 'vue';

interface Props {
  total: number;
  perPageItems?: number[];
  showTotal?: boolean;
}

export default {
  title: 'Library/Components/Pagination',
  component: AppPagination,
} as Meta<typeof AppPagination>;

const Template: StoryFn<Props> = (args) => ({
  setup() {
    const pagination = ref({
      page: 1,
      pageSize: 20,
    });

    return { args, pagination };
  },
  template: `
    <AppPagination
      v-bind="args"
      v-model="pagination"
    />
  `,
});

export const Default = Template.bind({});
Default.args = {
  total: 100,
};
