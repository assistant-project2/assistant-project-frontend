import Button from '../AppButton.vue';
import { Meta, StoryFn } from '@storybook/vue3';

interface ButtonProps {
  label: string;
  secondary?: boolean;
  transparent?: boolean;
  stroked?: boolean;
}

export default {
  title: 'Library/Components/Button',
  component: Button,
} as Meta<typeof Button>;

const Template: StoryFn<ButtonProps> = (args) => ({
  components: { Button },
  setup() {
    return { args };
  },
  template: `
    <Button v-bind="args">
      ${args.label}
    </Button>
  `,
});

export const Primary = Template.bind({});
Primary.args = {
  label: 'Label',
};

export const Secondary = Template.bind({});
Secondary.args = {
  ...Primary.args,
  secondary: true,
};

export const Transparent = Template.bind({});
Transparent.args = {
  ...Primary.args,
  transparent: true,
};

export const Outline = Template.bind({});
Outline.args = {
  ...Primary.args,
  stroked: true,
};
