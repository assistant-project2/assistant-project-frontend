export type LocalStorageValueType = string | number | boolean;

export const enum LocalStorageKeys {
  'AUTH_TOKEN' = 'auth_token',
}

export default class LocalStorageService {
  public static setItem<T extends LocalStorageValueType>(key: string, value: T): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  public static getItem<T extends LocalStorageValueType>(key: string): T | null;
  public static getItem<T extends LocalStorageValueType>(key: string, otherwise: T): T;
  public static getItem<T extends LocalStorageValueType>(key: string, otherwise?: T): T | null {
    const data: string | null = localStorage.getItem(key);
    return data ? JSON.parse(data) : otherwise ?? null;
  }

  public static removeItem(key: string): void {
    localStorage.removeItem(key);
  }
}
