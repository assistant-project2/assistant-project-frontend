import { nextTick } from 'vue';
import { createI18n, I18nOptions, I18n } from 'vue-i18n';
import { buildYupLocale } from '@/core/plugins/yup';
import Api from './Api';

export const SUPPORT_LOCALES = ['en', 'ru'];

export let i18n: I18n & { global: { t: (val: string) => string } };

export async function setupI18n() {
  const browserLanguage = navigator.language.split('-')[0];
  const locale = SUPPORT_LOCALES.includes(browserLanguage) ? browserLanguage : 'en';

  const options: I18nOptions = {
    legacy: false,
    locale: locale,
    fallbackLocale: 'en',
    globalInjection: true,
  };
  i18n = createI18n(options);

  setI18nLanguage(i18n, locale);
  await loadLocaleMessages(i18n, locale);

  return i18n;
}

export function setI18nLanguage(i18n: any, locale: string) {
  i18n.global.locale.value = locale;
  Api.getInstance.setLanguage(locale);
}

export async function loadLocaleMessages(i18n: any, locale: string) {
  const messages = await import(`../../assets/locale/${locale}.json`);

  i18n.global.setLocaleMessage(locale, messages.default);
  buildYupLocale(i18n);

  return nextTick();
}
