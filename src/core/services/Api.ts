import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios';
import router from '@/router';
import { ErrorBase } from '../types/base';
import LocalStorageService, { LocalStorageKeys } from './LocalStorage';
import useModalConfirm from '@/core/plugins/useModalConfirm';
import { ModalConfirmTypes } from '@/components/modal/types';
import { i18n } from '@/core/services/I18n';

interface IFile {
  url: string;
}

export interface Pagination {
  page: number; // Номер текущей страницы.
  page_size: number; // Сколько элементов на одной странице.
  total_count: number; // Сколько всего элементов.
}

export type PaginationRequest = Omit<Pagination, 'total_count'>;

// export interface ResponseSuccessBody<T = unknown> {
//   pagination?: Pagination;
//   result: T;
// }

// export interface ResponseErrorDescriptionItem {
//   loc: string[];
//   msg: string;
// }

interface ResponseErrorBody {
  detail?: string;
  non_field_errors: string[];
}

export type ApiErrorName = 'RESPONSE_ERROR';

export class ApiError extends ErrorBase<ApiErrorName> {
  code: number;

  constructor(name: ApiErrorName, statusCode: number, statusText: string) {
    super(name, `${statusCode} ${statusText}`);
    this.code = statusCode;
  }
}

export abstract class HttpClient {
  protected readonly instance: AxiosInstance;

  protected constructor(baseURL: string) {
    const config: AxiosRequestConfig = {
      baseURL,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };

    this.instance = axios.create(config);
    this._initializeRequestInterceptor();
    this._initializeResponseInterceptor();
  }

  private _initializeRequestInterceptor(): void {
    this.instance.interceptors.request.use(this._handleRequest);
  }

  private _initializeResponseInterceptor(): void {
    this.instance.interceptors.response.use(this._handleResponse, this._handleError);
  }

  protected _handleRequest(config: AxiosRequestConfig) {
    const token = LocalStorageService.getItem<string>(LocalStorageKeys.AUTH_TOKEN);

    if (!token) {
      return config;
    }

    return {
      ...config,
      headers: {
        ...config.headers,
        Authorization: `Token ${token}`,
      },
    };
  }

  protected _handleResponse({ data }: AxiosResponse) {
    return data;
  }

  protected async _handleError(error: AxiosError<ResponseErrorBody>) {
    const confirmError = useModalConfirm(`${i18n.global.t('error')}!`, `${i18n.global.t('error_save')}!`, {
      onlyConfirm: true,
      clickToClose: false,
      type: ModalConfirmTypes.ERROR,
    });

    // Network error
    if (!error.response) {
      confirmError.open();
      return Promise.reject(error);
    }

    const { status, data: body } = error.response;
    const data = body;

    if (status >= 500) {
      await router.push({ name: '500' });
    } else if (status === 404) {
      await router.push({ name: '404' });
    } else if (status === 401) {
      await router.push({ name: 'login' });
    } else {
      await confirmError.open({
        title: 'Ошибка',
        subTitle: data.detail || 'Что-то пошло не так',
      });

      const apiError = new ApiError('RESPONSE_ERROR', status, data.detail || 'Что-то пошло не так');
      return Promise.reject(apiError);
    }
  }

  public setLanguage(locale: string): void {
    this.instance.defaults.headers.common['Accept-Language'] = locale;
  }

  public uploadFile<T = IFile>(
    url: string,
    file: File,
    otherData?: Record<string, string>,
    config?: AxiosRequestConfig,
    field = 'file'
  ): Promise<T> {
    const formData = new FormData();
    file && formData.append(field, file);

    for (const key in otherData) {
      formData.append(key, otherData[key]);
    }

    return this.instance.post<never, T>(url, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    });
  }

  public uploadFileUpdate<T = IFile>(
    url: string,
    file: File,
    id: number,
    otherData?: Record<string, string>,
    config?: AxiosRequestConfig,
    field = 'file'
  ): Promise<T> {
    const formData = new FormData();
    file && formData.append(field, file);

    for (const key in otherData) {
      formData.append(key, otherData[key]);
    }

    return this.instance.put<never, T>(`${url}/${id}/`, formData, {
      ...config,
      headers: {
        ...config?.headers,
        'Content-Type': 'multipart/form-data',
      },
    });
  }

  public get<T>(url: string, params?: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.get<never, T>(url, {
      ...config,
      params,
    });
  }

  public getByID<T>(url: string, id: number, params?: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.get<never, T>(`${url}/${id}`, {
      ...config,
      params,
    });
  }

  public post<T>(url: string, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.post<never, T>(url, data, config);
  }

  public update<T>(url: string, id: number, data: unknown, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.put<never, T>(`${url}/${id}/`, data, config);
  }

  public delete<T>(url: string, id: number, config?: AxiosRequestConfig): Promise<T> {
    return this.instance.delete<never, T>(`${url}/${id}/`, config);
  }
}

export default class Api extends HttpClient {
  private static classInstance?: Api;

  private constructor() {
    const baseURL = `${import.meta.env.VITE_APP_API_URL}/`;
    super(baseURL);
  }

  public static get getInstance(): Api {
    if (!this.classInstance) {
      this.classInstance = new Api();
    }
    return this.classInstance;
  }
}
