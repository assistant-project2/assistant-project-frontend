import * as Yup from 'yup';

export function buildYupLocale(i18n: any) {
  Yup.setLocale({
    mixed: {
      required: i18n.global.t('components.form.field.required'),
    },
    string: {
      // email: i18n.global.t('email_form_field_valid'),
    },
  });
}
