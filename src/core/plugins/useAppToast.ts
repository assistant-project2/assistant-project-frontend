import { useToast, PluginOptions } from 'vue-toastification';
import AppToast from '@/components/AppToast.vue';

const toastBase = { component: AppToast };
const defaultOptions: PluginOptions = {
  timeout: false,
  toastClassName: '!p-0 !overflow-visible !bg-transparent !min-h-0',
  icon: false,
  closeButton: false,
};

export function useAppToast({ timeout = 4000 } = {}) {
  const toast = useToast();

  return {
    success(text: string) {
      const id = toast({ ...toastBase, props: { text } }, defaultOptions);
      setTimeout(() => toast.dismiss(id), timeout);
    },
  };
}
