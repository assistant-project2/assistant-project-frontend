import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import LocalStorageService, { LocalStorageKeys } from '@/core/services/LocalStorage';
import Api from '@/core/services/Api';

export interface IUser {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  re_password: string;
}

export default defineStore('account', () => {
  // State
  const account = ref<IUser>({} as IUser);
  const isUserAuthenticated = ref<boolean>(!!LocalStorageService.getItem<string>(LocalStorageKeys.AUTH_TOKEN));

  // Getters
  const getAccount = computed<IUser | null>(() => {
    const token = LocalStorageService.getItem<string>(LocalStorageKeys.AUTH_TOKEN);

    return token ? account.value : null;
  });

  // Actions
  const logOut = () => {
    LocalStorageService.removeItem(LocalStorageKeys.AUTH_TOKEN);
    isUserAuthenticated.value = false;
  };

  const logIn = async (credentials: IUser) => {
    const { auth_token } = await Api.getInstance.post<{ auth_token: string }>('auth/token/login/', credentials);
    LocalStorageService.setItem<string>(LocalStorageKeys.AUTH_TOKEN, auth_token);
    isUserAuthenticated.value = true;
  };

  const signUp = async (credentials: IUser) => {
    await Api.getInstance.post('auth/users/', credentials);
  };

  const activation = async (credentials: unknown) => {
    try {
      await Api.getInstance.post('auth/users/activation/', credentials);
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const fetchUser = async () => {
    const response = await Api.getInstance.get<IUser>('auth/users/me/');

    account.value = response;
  };

  return {
    getAccount,
    isUserAuthenticated,
    logOut,
    logIn,
    signUp,
    activation,
    account,
    fetchUser,
  };
});
