import { defineStore } from 'pinia';
import { ref } from 'vue';
import Api from '@/core/services/Api';
import { IUser } from './account';
import { IData } from '@/core/types/base';

export interface IBlock {
  id: number;
  course: number;
  title: string;
  description: string;
  status: number;
  file?: string | File;
}

export interface IAccountCourse {
  id: number;
  role: 'Supervisor' | 'Assistant' | 'Student';
  team?: ITeam;
}

export interface ICourse {
  id: number;
  title: string;
  subtitle: string;
  description: string;
  image?: string;
  blocks: IBlock[];
  account_course: IAccountCourse;
}

export interface ITeam {
  id: number;
  title: string;
  members: IUser[];
  course: number;
}

export interface IAccountBlock extends IData {
  id: number;
  block: number;
  account: IUser;
  team?: ITeam | string;
  file?: string | File;
  score?: number;
  assistant?: IUser | string;
  comment?: string;
}

export default defineStore('course', () => {
  // State
  const course = ref<ICourse>({} as ICourse);
  const courses = ref<ICourse[]>([]);
  const accountBlocks = ref<IAccountBlock[]>([]);

  // Getters

  // Actions
  const fetchCourseList = async () => {
    try {
      const response = await Api.getInstance.get<ICourse[]>('courses/');

      courses.value = response;
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const fetchCourse = async (id: number) => {
    try {
      const response = await Api.getInstance.getByID<ICourse>('course', id, {
        course: id,
      });

      course.value = response;
      course.value.blocks.sort((a, b) => b.id - a.id);
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const createBlock = async (body: IBlock) => {
    try {
      const file = body.file;
      delete body['file'];
      body.course = course.value.id;

      const response = await Api.getInstance.uploadFile<IBlock>(
        'blocks/',
        file as File,
        body as unknown as Record<string, string>
      );

      course.value.blocks.push(response);
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const updateBlock = async (body: IBlock, id: number) => {
    try {
      const file = body.file;
      delete body['file'];
      body.course = course.value.id;

      const response = await Api.getInstance.uploadFileUpdate<IBlock>(
        'blocks',
        file as File,
        id,
        body as unknown as Record<string, string>
      );

      const blockIndex = course.value.blocks.findIndex((item) => item.id === response.id);

      if (blockIndex != -1) {
        course.value.blocks[blockIndex] = {
          ...response,
        };
      }
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const fetchAccountsBlock = async (id: number) => {
    try {
      const response = await Api.getInstance.get<IAccountBlock[]>('account_blocks/', {
        block: id,
        course: course.value.id,
      });

      accountBlocks.value = response;
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const createAccountBlock = async (id: number, file: File) => {
    try {
      const otherData: Record<string, string> = {
        course: String(course.value.id),
        block: String(id),
      };

      const response = await Api.getInstance.uploadFile<IAccountBlock>('account_blocks/', file, otherData);

      accountBlocks.value = [response];
    } catch (error) {
      console.log('error :>> ', error);
    }
  };

  const setScore = async (id: number, data: IAccountBlock, block: number) => {
    const response = await Api.getInstance.update<IAccountBlock>('account_blocks', id, {
      ...data,
      block,
      course: course.value.id,
    });

    const blockIndex = accountBlocks.value.findIndex((item) => item.id === response.id);

    if (blockIndex != -1) {
      accountBlocks.value[blockIndex] = {
        ...response,
      };
    }
  };

  const createMember = async (course: number, role: string) => {
    if (role !== 'Assistant' && role !== 'Student') {
      return;
    }

    await Api.getInstance.post('courses/member/', {
      course,
      role,
    });
  };

  const createTeamMember = async (course: number, team: number) => {
    await Api.getInstance.post('teams/member/', {
      team,
      course,
    });
  };

  const createTeam = async (body: ITeam) => {
    const response = await Api.getInstance.post<ITeam>('teams/', {
      ...body,
      course: course.value.id,
    });

    course.value.account_course.team = response;
  };

  return {
    course,
    courses,
    fetchCourseList,
    fetchCourse,
    createBlock,
    updateBlock,
    fetchAccountsBlock,
    accountBlocks,
    createAccountBlock,
    setScore,
    createMember,
    createTeam,
    createTeamMember,
  };
});
