import { RouteRecordRaw } from 'vue-router';

// main
import error404 from '@/router/pages/main/Error404';
import error500 from '@/router/pages/main/Error500';
import MainLayout from '@/layouts/MainLayout.vue';

// course
import CourseListing from '@/router/pages/courses/listing';
import CouseDetail from '@/router/pages/courses/detail';
import BlockDetail from '@/router/pages/courses/block';

// account
import AuthLayout from '@/views/account/AuthLayout.vue';
import LogIn from '@/router/pages/account/login';
import SignUp from '@/router/pages/account/signup';
import Activation from '@/router/pages/account/activation';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'main',
    component: MainLayout,
    children: [CourseListing, CouseDetail, BlockDetail],
  },
  {
    path: '/auth',
    component: AuthLayout,
    children: [LogIn, SignUp, Activation],
  },
  error404,
  error500,
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
];

export default routes;
