import route from '@/views/courses/CourseListing.vue';

const routes = {
  path: '/',
  name: 'course-listing',
  component: route,
};

export default routes;
