import route from '@/views/courses/CourseDetail.vue';

const routes = {
  path: 'course/:id',
  name: 'course-detail',
  component: route,
};

export default routes;
