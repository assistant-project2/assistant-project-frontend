import route from '@/views/courses/BlockDetail.vue';

const routes = {
  path: 'course/:id/block/:block_id?',
  name: 'block-detail',
  component: route,
};

export default routes;
