import route from '@/views/account/AuthActivation.vue';

const routes = {
  path: 'activation/:uid/:token',
  name: 'activation',
  component: route,
};

export default routes;
