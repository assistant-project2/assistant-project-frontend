import { createRouter, createWebHistory } from 'vue-router';
import routes from '@/router/routes';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  /*const token = localStorage.getItem('token');

    if (to.path === '/login') {
        token ? next('/') : next();
        return;
    }
    if (!token) {
        next('/login');
        return;
    }*/
  next();
});

export default router;
